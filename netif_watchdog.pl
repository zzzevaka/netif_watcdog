#!/usr/bin/env perl
#
# DESCRIPTION: watchdog script for FreeBSD network interfaces
# AUTHOR: maksimov
# VERSION: 1.10
# USAGE: look at $usage or use -h

package main;

use strict;
use warnings;
use Storable;
use POSIX('strftime');
use Getopt::Long qw(:config bundling);
use Pod::Usage;
use Data::Dumper;

#################################################
# constants
#################################################

# interface names
my @NAMES = qw/em re fxp dc rl sk/;
my $NETSTAT_REGEXP = '^(' . (join '|', @NAMES) . ')[0-9]{1,2}\b.+Link';
# commands
my $IFCONFIG = '/sbin/ifconfig';
my $NETSTAT = '/usr/bin/netstat';
my $MAIL = '/usr/bin/mail';
# for mail
chomp(my $HOSTNAME = `/bin/hostname`);
my $HOSTMASTER = 'root';

##################################################
# options
##################################################
# interface names which user defined
my @names_user_defined = ();
# file to serialize data about interfaces
# betweeen runnings
my $serialize_file = '/tmp/netif_watchdog.dump';
# a period while listening netstat (-w)
my $netstat_wait = 10;
# min threshold for an active interface 
my $input_packets_active = 500;
my $output_packets_active = 500;
# how many times threshold must be reached to mark an interface as active
my $times_to_active = 5;
# stuck thresholds.
my $input_packets_stuck = -1;
my $output_packets_stuck = 0;
# how many times script must do down/up before an interface will be ignored
my $dead_times_to_ignore = 2;
# ignored interfaces
my @ignore = ();
# email vlaues
my $send_email = 1;
my $email_subject = "netif_watchdog: $HOSTNAME";
my $email_to = $HOSTMASTER;

# other flags
my $help;
my $debug;
my $real_time_log;

##################################################
# parsing of command line options 
##################################################

my $usage = "Usage: $0 [OPTIONS]\n\n" .
            "DECRIPTION:\n\n" .
            "  the script listens netstat and does `ifconfig <ifname> down/up` if:\n" .
            "    -there is physical link on the interface\n" .
            "    -there is UP flag on the interface\n" .
            "    -there are routes on the interface, it's vlans or bridges\n" .
            "    -normal traffic has been fixed on the interface lately (active interface)\n" .
            "\n" .
            "  the script handles interfaces with names: @NAMES\n" .
            "\n" .
            "OPTIONS:\n\n" .
            "  -h,  --help\t\t\tthis page\n" .
            "  -d,  --debug\t\t\tenable debug\n" .
            "  -n,  --iface_name\t\tdefine a name of an interface for handling\n" .
            "  \t\t\t\t  by default all physical interfaces are handled\n" .
            "  -i,  --ignore=name\t\tignored interface. do not use it if flag -n is specified\n" .
            "  -w,  --netstat-wait=seconds\tdelay between netstat measurements\n" .
            "  \t\t\t\t  by default $netstat_wait seconds\n" .
            "  --history-file=file\t\tfile for saving history betweeen runnings\n" .
            "  \t\t\t\t  by default $serialize_file\n" .
            "  --input-packet-active=number\thow many input packets must be on an active interface\n" .
            "  \t\t\t\t  by default $input_packets_active packets\n" .
            "  --output-packet-active=number\thow many output packets must be on an active interface\n" .
            "  \t\t\t\t  by default $output_packets_active packets\n" .
            "  --times-to-active=number\thow many times input and output thresholds must be reached to mark an interface as active\n" .
            "  \t\t\t\t  by default $times_to_active times\n" .
            "  --input-packets-stuck=number\tif input packets less than or equal to this value on an active interface - interface is stuck\n" .
            "  \t\t\t\t  by default $input_packets_stuck packets\n" .
            "  --output-packets-stuck=number\tif output packets less than or equal to this value on an active interface - interface is stuck\n" .
            "  \t\t\t\t  by default $output_packets_stuck packets\n" .
            "  --ignore-after-dead=number\thow many times script must do down/up before the interface will be ignored\n" .
            "  \t\t\t\t  by default $dead_times_to_ignore times\n" .
            "  --disable-email\t\tby default the script sends email (by $MAIL) if some interface is stuck.\n" .
            "  \t\t\t\t  define this flag to disable this behaviour\n" .
            "  --email-subject=subject\temail subject. by default '$email_subject'\n" .
            "  --email-to=email\t\temail recipient. by default '$email_to'\n" .
            "\n" .
            "EXAMPLES:\n\n" .
            "  $0 -i em0 -i re1\n" .
             "  $0 -n rl2 -n rl1\n" .
            "  $0 -w 20 --check-before-reset=10\n";

GetOptions ("help|h" => \$help,
            "debug|d" => \$debug,
            "ignore|i=s"  => \@ignore,
            "iface-name|n=s"  => \@names_user_defined,
            "history-file=s" => \$serialize_file,
            "netstat-wait|w=s" => \$netstat_wait,
            "input-packet-active=i" => \$input_packets_active,
            "output-packet-active=i" => \$output_packets_active,
            "times-to-active=i" => \$times_to_active,
            "input-packets-stuck=i" => \$input_packets_stuck,
            "output-packets-stuck=i" => \$output_packets_stuck,
            "ignore-after-dead=i" => \$dead_times_to_ignore,
            "email-subject=s" => \$email_subject,
            "email-to=s" => \$email_to,
            ) or pod2usage($usage);
pod2usage($usage) if $help;

##################################################
# main program
##################################################

my $exit_value = 0;
my $interfaces = {};

# global eval block
eval {
    # get all interface names
    my ($code, $out) = exec_cmd("$IFCONFIG -l");
    die "couldn't get 'ifconfig -l':\n$out" if $code;
    my @all_names = split ' ', $out;
    # get interface names which must be handled
    my @to_handling = @names_user_defined ? @names_user_defined : @all_names;
    foreach my $ign (@ignore) {
        @to_handling = grep $_ ne $ign, @to_handling;
    }
    # try to deserialize interfaces
    eval {
        $interfaces = retrieve($serialize_file);
        # delete ignored interfaces
        foreach my $ifname (keys %$interfaces) {
            if (!grep {$_ eq $ifname} @to_handling) {
                buff_log("delete $ifname", lvl=>'d') if $debug;
                delete $interfaces->{$ifname};
            }
        }
    };
    if ($@) {
        chomp($@);
        buff_log("WARNING: couldn't load $serialize_file ($@)", lvl=>'d');
    }
    buff_log("TO_HANDLING: @to_handling", lvl=>'d');
    # initialize interfaces if they haven't been already initialized
    foreach my $ifname (@to_handling) {
        # skip an existing interface
        next if defined($interfaces->{$ifname});
        # initialize an interface
        my $netif;
        eval {
            $netif = NetIf->new($ifname);
        };
        if ($@) {
            buff_log(
                "An error has occured while handling netif $ifname:\n$@",
                lvl=>'e'
            );
            $exit_value = 1;
            next;
        }
        # ignore undefined
        if (!defined($netif)) {
            next;
        }
        $interfaces->{$ifname} = $netif;
    }
    # checking
    # all interfaces which must be checked
    my @need_to_reset = ();
    buff_log("start checking", lvl=>'d');
    my @stat = get_netstat();
    buff_log(
        "----------- NETSTAT -----------\n" .
        Dumper(\@stat)
    );
    foreach my $iface (grep {$_->is_up()} values %$interfaces) {
        my $name = $iface->name();
        my $idelta = $stat[1]->{$name}{'ipkts'} - $stat[0]->{$name}{'ipkts'};
        my $odelta = $stat[1]->{$name}{'opkts'} - $stat[0]->{$name}{'opkts'};
        if ($idelta < 0 || $odelta < 0) {
            buff_log(
                "Incorrect netstat data for an interface $name:\n" .
                "Input delta: $idelta\n".
                "Output delta: $odelta"
            );
            $exit_value = 1;
            next;
        }
        # min thresholds for an active interface are reached
        # increment active flag
        if (
            $idelta >= $input_packets_active &&
            $odelta >= $output_packets_active
        ) {
            $iface->{'active'} += 1;
            $iface->{'dead'} = 0;
            buff_log($name . " is active", lvl=>'d');
        }
        # stuck threshold is reached. if an interface is active do down/up 
        elsif (
            $iface->{'active'} >= $times_to_active &&
            ($idelta <= $input_packets_stuck || $odelta <= $output_packets_stuck)
        ) {
            buff_log($name . " must be reseted", lvl=>'d');
            push @need_to_reset, $iface;
        }
        # interface isn't active ant isn't stuck. decrement active flag
        else {
            buff_log($name . " isn't active", lvl=>'d');
            $iface->{'active'}-- if $iface->{'active'} > 0;
        }
    }
    # resetting
    foreach my $iface (@need_to_reset) {
        # deactive the interface if it's dead more than
        # $dead_times_to_ignore checkings in a row
        if ($iface->{'dead'}++ >= $dead_times_to_ignore) {
            $iface->{'active'} = 0;
            buff_log($iface->name() . " ignored", lvl=>'d');
        }
        # do down/up if the interface is active
        else {
            $iface->down();
            sleep(1);
            $iface->up();
            buff_log($iface->name() . " restarted");
            $exit_value = 1;
        }
    }

    # serialize interfaces
    store($interfaces, $serialize_file);
    
    # log to email if something wrong
    if ($exit_value) {
        open(MSG, "| $MAIL -s '$email_subject' '$email_to'")
            or die "couldn't send email ($@)";
        print MSG get_log_buffer();
        close(MSG);
    }
};
if ($@) {
    buff_log("An error has occurred: $@", lvl=>'e');
    $exit_value = 1;
}

exit($exit_value);

# THE END

##################################################
# main functions
##################################################

# execute command with redirection STDERR to STDOUT
sub exec_cmd {
    my $cmd = shift;
    buff_log("CMD $cmd", lvl=>'d');
    my $stdout = `$cmd 2>&1`;
    return wantarray ? ($?, $stdout) : $?;
}

# logging
{
    my $log_buffer = '';
    sub buff_log {
        chomp(my $msg = shift);
        my %opt = @_;
        my $lvl = defined($opt{'lvl'}) ? $opt{'lvl'} : 'i';
        if ($lvl eq 'd' and !$debug) {
            return 0;
        }
        my $now = strftime "%Y-%m-%d %T", localtime;
        $msg = ("ERROR: " . $msg) if $lvl eq 'e';
        $msg = ("DEBUG: " . $msg) if $lvl eq 'd';
        $msg = "$now: $msg\n";
        print "$msg";
        $log_buffer .= "$msg";
        return 1;
    }
    sub get_log_buffer {
        return defined($log_buffer) ? $log_buffer : '';
    }
}

# get netstat -nWdi
sub get_netstat {
    my @measures = ();
    for (my$i=0;$i<2;$i++) {
        my ($code, $out) = exec_cmd(
            $NETSTAT . " -nWdi | grep -E '$NETSTAT_REGEXP'"
        );
        die "couldn't get netstat ($out)" if $code;
        $measures[$i] = {};
        foreach (split '\n', $out) {
            my @val = split ' +';
            $measures[$i]->{$val[0]} = {ipkts => $val[4], opkts => $val[7]};
        }
        sleep($netstat_wait) if $i < 1;
    }
    return wantarray ? @measures : \@measures;
}

##################################################
# class NetIf - network interface
##################################################

package NetIf;

# constructor
sub new {
    my $class = shift;
    my $self = bless {}, $class;
    $self->{'full_name'} = shift or die "netif name isn't defined";
    # split interface name on prefix and number.
    if ($self->{'full_name'} =~ m/^([a-z]+)(\d+)$/) {
        ($self->{'prefix'}, $self->{'number'}) = ($1, $2);
    }
    else {
        die "incorrect netif name: " . $self->{'full_name'};
    }
    # return undef if prefix isn't in @NAMES
    if (!grep $self->{'prefix'} eq $_, @NAMES) {
        return undef;
    }
    $self->get_ifconfig();
    # an interface is inactive by default
    $self->{'active'} = 0;
    # how many checkings in row an interface is dead
    $self->{'dead'} = 0;
    return $self;
}

# get an interface name
sub name {
    my $self = shift;
    return $self->{'full_name'};
}

# execute and parse 'ifcofnig <netif name>'
sub get_ifconfig {
    my $self = shift;
    my $cmd = $IFCONFIG ." ". $self->{'full_name'};
    my ($code, $out) = main::exec_cmd($cmd);
    die "couldn't get ifconfig:\ncmd: $cmd\nstdout+stderr: $out" if $code;
    $self->{'flags'} = $out =~ m/flags=.+<(.*)>/ ?  [split ',', $1] :
        die "couldn't get flags. ifconfig output: $out";
    $self->{'ether'} = $1 if $out =~ m/ether ([0-9a-f:]+)/;
    $self->{'status'} = $out =~ m/status: ([\w ]+)$/m ? $1 :
        die "couldn't get netif status. ifconfig output: $out";
    $self->{'has_link'} = $self->{'status'} eq 'active' ? 1 : 0;
    $self->{'bridge_member'} = [];
    return 1;
}

# find vlan on an interface
sub load_vlans {
    my $self = shift;
    my ($code, $out) = main::exec_cmd(
        "ifconfig | grep 'parent interface: " . $self->name . "'"
    );
    $self->{'vlans'} = [];
    # if $code != 0 - grep didn't find any vlans
    return 1 if $code;
    foreach (split '\n', $out) {
        push @{$self->{'vlans'}}, / ?vlan: (\d{1,4}) parent.+/;
    }
    return 1;
}

# an interface is up if:
# - it has link;
# - it has a flag "UP"
# - there are routes on this interface or on it`s vlans
sub is_up {
    my $self = shift;
    # refresh interface info
    $self->get_ifconfig();
    # if the interface doesn't have flag 'UP'
    if (grep m/^UP$/, @{$self->{'flags'}}) {
        main::buff_log("flag 'UP': +" , lvl=>'d');
    }
    else {
        main::buff_log("flag 'UP': -", lvl=>'d');
        return 0;
    }
    # if the interface doesn't have physical link
    if ($self->{'has_link'}) {
        main::buff_log("link: +", lvl=>'d');
    }
    else {
        main::buff_log("link: -", lvl=>'d');
        return 0;
    }
    # refresh vlans
    $self->load_vlans();
    # regexp for routes searching
    my $regexp = join '|', $self->name(), map {'vlan'.$_.'\b'} @{$self->{'vlans'}};
    # is interface or vlans are members in a bridge
    my ($code, $out) = main::exec_cmd('ifconfig -l | grep bridge');
    # if bridge was finded
    if (!$code) {
        my @bridges = grep m/^bridge/, split (' ', $out);
        # try to find self in bridge members
        foreach (@bridges) {
            if (!main::exec_cmd($IFCONFIG . " $_ | grep -E 'member: ($regexp)'")) {
                push @{$self->{'bridge_member'}}, $_;
            }
        }
    }
    $regexp = join '|', (@{$self->{'bridge_member'}}, $regexp);
    if (!main::exec_cmd($NETSTAT . " -Wrn | grep -E '$regexp'")) {
        main::buff_log("routes: +", lvl=>'d');
    }
    else {
        main::buff_log("routes: -\n", lvl=>'d');
        return 0;
    }
    main::buff_log("DEBUG: " . $self->name . " is up", lvl=>'d');
    return 1;
}

# ifconfig <netif name> down
sub down {
    my $self = shift;
    main::buff_log($self->name . " down\n", lvl=>'d');
    my ($code, $out) = main::exec_cmd($IFCONFIG . " " . $self->name() . " down");
    if ($code) {
        die "couldn't down an interface " . $self->name() . ":n$out";
    }
    return 1;
}

# ifconfig <netif name> up
sub up {
    my $self = shift;
    main::buff_log($self->name . " up\n", lvl=>'d');
    my ($code, $out) = main::exec_cmd($IFCONFIG . " " . $self->name() . " up");
    if ($code) {
        die "couldn't up an interface " . $self->name() . ":n$out";
    }
    return 1;
}
